# Digital Field Modeling

## Introduction

The aim of this project is to propose a solution for the generation of a 2D image representing, by color levels, the different altitudes of a 2.5D terrain. An algorithm coded in C++ will be proposed to solve this problem and display 2 maps in 2.5D. To use it, clone this repository and follow the next commands :

```
./build.sh
cd build
./create_raster <nameofthetxtfile.txt> <widthresolution>
```

For instance, to use the **Guerledan.txt** file, with a 1024 resolution in width :

```
./create_raster Guerledan.txt 1024
```

## Meshing

A DTM rendering is an image in which each pixel corresponds to a squared area in the chosen projection. Generating this image involves calculating a color for each pixel, synthesizing the corresponding DTM elevations of the squared area. DTM points can form a regular grid. In this case, the association between image pixels and the points in the DTM file can seem simple. When the grid is not regular, it is preferable to create a mesh using, for example, a Delaunay  triangulation algorithm, which is done in this project.

![alt text](image.png)

## Optimizing

A naive search for the triangle or triangles in the mesh, corresponding to a pixel in the image, is a complex operation. The n triangles in the mesh are scanned and tested, and only those containing the projected pixel are retained. In cases where millions of triangles model the terrain, several hours of computation may be necessary to generate the image. An efficient solution is to pave the space by dichotomy, to distribute the triangles into the leaves of a binary search tree : 

![alt text](image-1.png)

## Coloring

To color the pixels, a haxby scale is used. Moreover, hill shading is used to perform a better image generation.

To add relief, we need to know the sun's position, based on two pieces of information two pieces of information: its azimuth (in rad) and its altitude (in rad). The first angle lets us know "which side" of the map the sun's rays are coming from, and the second to know "how high" the triangles are illuminated.

Assumption: All the sun's rays considered are parallel. This is because we'll assume that the sun's altitude doesn't change from one triangle to the next, which would require different calculations for each triangle and would make the calculation more cumbersome, all the more so since, for the size of map considered (dimensions of a few meters), this hypothesis remains entirely plausible :

```cpp
const double sunAzimuth = 0 ;
const double sunAltitude = M_PI/4 ;
double sunDirectionX = cos(sunAzimuth) ;
double sunDirectionY = sin(sunAzimuth) ;
double sunDirectionZ = sin(sunAltitude) ;

vector<double> normale = produitvectoriel(p1, p2, p3) ;
normale = normaliser(normale) ;
double nx, ny, nz ;
nx = normale[0] ;
ny = normale[1] ;
nz = normale[2] ;

double illuminationFactor = nx * sunDirectionX + ny * sunDirec
if (illuminationFactor > 0.0) {
    illuminationFactor = 0.0 ;
}

double z = altitudeMoyenne/nbtri ;
int r, g, b;
haxby(r, g, b, z, zmin, zmax);
fichierPGM.put(static_cast<char>(r * (1 + 0.5 * hillShadeFactor)));
fichierPGM.put(static_cast<char>(g * (1 + 0.5 * hillShadeFactor)));
fichierPGM.put(static_cast<char>(b * (1 + 0.5 * hillShadeFactor))) ;
```

## Results

**File** : Guerledan.txt, 1024

![alt text](image-2.png)

**File** : rade.txt, 1024

![alt text](image-3.png)