#include "Point.h"
#include <cmath>


Point::Point(double x, double y, double z) : x(x), y(y), z(z)
{
}

Point::Point(double x, double y) : x(x), y(y) 
{
    z = 0 ;
}

Point::~Point()
{
}

// Vérification si le point créé appartient à un triangle 
bool Point::pointDansTriangle(const Point& P1, const Point& P2, const Point& P3) {
  // Calculer les barycentres
  double alpha = ((P2.y - P3.y)*(x - P3.x) + (P3.x - P2.x)*(y - P3.y)) /
    ((P2.y - P3.y)*(P1.x - P3.x) + (P3.x - P2.x)*(P1.y - P3.y));
  double beta = ((P3.y - P1.y)*(x - P3.x) + (P1.x - P3.x)*(y - P3.y)) /
    ((P2.y - P3.y)*(P1.x - P3.x) + (P3.x - P2.x)*(P1.y - P3.y));
  double gamma = 1.0 - alpha - beta;
  return (alpha >= 0 && beta >= 0 && gamma >= 0);
}

vector<double> Point::vecteur(const Point& P1, const Point& P2) {
    vector<double> resultat(3) ;
    resultat[0] = P2.x - P1.x ;
    resultat[1] = P2.y - P1.y ;
    resultat[2] = P2.z - P1.z ;
    return resultat ;
}

vector<double> Point::produitvectoriel(const Point& P1, const Point& P2, const Point& P3) {
    vector<double> resultat(3) ;
    vector<double> v1 = vecteur(P1, P2) ;
    vector<double> v2 = vecteur(P1, P3) ;

    resultat[0] = v1[1]*v2[2] - v1[2]*v2[1] ;
    resultat[1] = v1[2]*v2[0] - v1[0]*v2[2] ;
    resultat[2] = v1[0]*v2[1] - v1[1]*v2[0] ;

    return resultat ;
}

vector<double> Point::normaliser(vector<double>& v) {
    double length = std::sqrt(v[0] * v[0] + v[1] * v[1] + v[2] * v[2]);
    vector<double> result(3);
    result[0] = v[0] / length;
    result[1] = v[1] / length;
    result[2] = v[2] / length;
    return result;
}