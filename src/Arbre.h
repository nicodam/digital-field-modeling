#ifndef __ARBRE_H__
#define __ARBRE_H__

#include "Noeud.h"
#include "Carte.h"
#include "Triangle.h"
#include <map>

using namespace std ;

/**
 * \class Arbre
 * \brief Création de l'arbre binaire utilisé pour la recherche dans l'image
 * \author Nicolas Damageux
 * \date 2023
*/

class Arbre
{

    /**
     * \brief Crée l'arbre une fois pour toute
     * \param racine Noeud d'origine de l'arbre
     * \param prondeurMax Profondeur de l'arbre, utilisée lors de la création et du découpage de la carte
    */

private:
    Noeud *racine;
    int profondeurMax;
public:
    Arbre(const Carte &racineCarte, int profondeur);
    ~Arbre();
    double calculerAltitudeMoyenne(Triangle* tr, const map<std::pair<double, double>, double>& altitudes) ;
    void diviserArbre(Noeud *noeud, int profondeurActuelle) ;
    vector<Triangle*> trouverzone(Noeud *noeud, int profondeurActuelle, double x, double y) ;
    void parcourirArbre(Noeud *noeud, int profondeurActuelle) ;
    void ajouterTriangle(Noeud* noeud, int profondeurActuelle, Triangle* tr, double x, double y) ;
    vector<Triangle*> trouverzone(double x, double y) ;
    void ajouterTriangle(Triangle* tr) ;
    void parcourirArbre() ;
    void diviserArbre() ;
    void detruireArbre(Noeud *noeud) ;

};


#endif