#ifndef __NOEUD_H__
#define __NOEUD_H__

#include "Carte.h"
#include "Triangle.h"
#include <vector>

using namespace std ;

/**
 * \class Noeud
 * \brief Un noeud de l'arbre binaire, pointant sur les deux noeuds suivants et contenant les triangles en faisant partie. Chaque noeud est associé à une carte (cf. class carte)
 * \author Nicolas Damageux
 * \date 2023
*/

class Noeud
{
    /**
     * \brief Crée un noeud de l'arbre binaire
     * \param carte Un objet de class carte, ie. une feuille de la carte originale
     * \param droit Un pointeur menant à la sous-carte suivante "droite"
     * \param gauche Un pointeur menant à la sous-carte suivante "gauche"
    */
public:
    Carte carte ;
    Noeud* droit ;
    Noeud* gauche ;
    vector<Triangle*> listeTriangle ;

    Noeud(const Carte &c);
    ~Noeud();
};



#endif