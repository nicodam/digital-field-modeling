#ifndef __TRIANGLE_H__
#define __TRIANGLE_H__

/**
 * \class Triangle
 * \brief Création d'un triangle
 * \author Nicolas Damageux
 * \date 2023
*/

class Triangle
{

    /**
     * \brief Chaque triangle est représenté par trois points
     * \param x1 Abscisse du premier point
     * \param y1 Ordonnée du premier point
     * \param x2 Abscisse du second point
     * \param y2 Ordonnée du second point
     * \param x3 Abscisse du dernier point
     * \param y3 Ordonnée du dernier point
    */

public:
    double x1, y1, x2, y2, x3, y3;

    Triangle(double x1, double y1, double x2, double y2, double x3, double y3);
    ~Triangle();
    void display() ;
} ;

#endif