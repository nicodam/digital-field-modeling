#include "Arbre.h"
#include <fstream>
#include <proj.h>
#include <fstream>
#include <list>
#include <map>
#include <chrono>

#include <algorithm>
#include <cmath>
#include <exception>
#include <iostream>
#include <limits>
#include <memory>
#include <utility>
#include <vector>

using namespace std ;

Arbre::Arbre(const Carte &racineCarte, int profondeur) : racine(new Noeud(racineCarte)), profondeurMax(profondeur) {}

Arbre::~Arbre() {}

double Arbre::calculerAltitudeMoyenne(Triangle* tr, const std::map<std::pair<double, double>, double>& altitudes) {
        double altMoyenne = 0.0;
        altMoyenne += altitudes.at({tr->x1, tr->y1});
        altMoyenne += altitudes.at({tr->x2, tr->y2});
        altMoyenne += altitudes.at({tr->x3, tr->y3});
        return altMoyenne / 3.0;
    }

void Arbre::diviserArbre(Noeud *noeud, int profondeurActuelle) {
    if (noeud == nullptr || profondeurActuelle >= profondeurMax) {
        //std::cout << "Feuille finale : (" << noeud->carte.xmin << "," << noeud->carte.ymin << "), (" << noeud->carte.xmax << "," << noeud->carte.ymax << ")" << std::endl ;
        return;
    }

    double largeur = noeud->carte.xmax - noeud->carte.xmin;
    double hauteur = noeud->carte.ymax - noeud->carte.ymin;

    if (largeur >= hauteur) {
        // Diviser selon la largeur
        double milieu = (noeud->carte.xmin + noeud->carte.xmax) / 2.0;
        Noeud *gauche = new Noeud(Carte(noeud->carte.xmin, milieu, noeud->carte.ymin, noeud->carte.ymax));
        Noeud *droite = new Noeud(Carte(milieu, noeud->carte.xmax, noeud->carte.ymin, noeud->carte.ymax));

        noeud->gauche = gauche;
        noeud->droit = droite;

        diviserArbre(gauche, profondeurActuelle + 1);
        diviserArbre(droite, profondeurActuelle + 1);
    } else {
        // Diviser selon la hauteur
        double milieu = (noeud->carte.ymin + noeud->carte.ymax) / 2.0;
        Noeud *haut = new Noeud(Carte(noeud->carte.xmin, noeud->carte.xmax, noeud->carte.ymin, milieu));
        Noeud *bas = new Noeud(Carte(noeud->carte.xmin, noeud->carte.xmax, milieu, noeud->carte.ymax));

        noeud->gauche = haut;
        noeud->droit = bas;

        diviserArbre(haut, profondeurActuelle + 1);
        diviserArbre(bas, profondeurActuelle + 1);
    }
}

vector<Triangle*> Arbre::trouverzone(Noeud *noeud, int profondeurActuelle, double x, double y) {
        if (noeud == nullptr || profondeurActuelle >= profondeurMax) {
            return noeud->listeTriangle ;
        }
        else{
            if (x >= noeud->gauche->carte.xmin && x <= noeud->gauche->carte.xmax && y >= noeud->gauche->carte.ymin && y <= noeud->gauche->carte.ymax) {
                return trouverzone(noeud->gauche, profondeurActuelle + 1, x, y) ;
            }
            else {
                return trouverzone(noeud->droit, profondeurActuelle + 1, x, y) ;
            }
        }
    }

void Arbre::parcourirArbre(Noeud *noeud, int profondeurActuelle) {
    if (noeud == nullptr || profondeurActuelle >= profondeurMax) {
        cout << "Feuille finale : (" << noeud->carte.xmin << "," << noeud->carte.ymin << "), (" << noeud->carte.xmax << "," << noeud->carte.ymax << ")" << endl ;
        for (auto tri : noeud->listeTriangle){
            std::cout << " - Triangle : " ;
            tri->display() ; std::cout << std::endl ;
            }
        std::cout << std::endl ;
        return;
    }
    else{
        parcourirArbre(noeud->gauche, profondeurActuelle + 1) ;
        parcourirArbre(noeud->droit , profondeurActuelle + 1) ;
    }
}

void Arbre::ajouterTriangle(Noeud* noeud, int profondeurActuelle, Triangle* tr, double x, double y) {
    if (noeud == nullptr || profondeurActuelle >= profondeurMax) {
        // Vérifier si le triangle est déjà présent dans la liste
        if (find(noeud->listeTriangle.begin(), noeud->listeTriangle.end(), tr) == noeud->listeTriangle.end()) {
            // Le triangle n'est pas dans la liste, on l'ajoute
            noeud->listeTriangle.push_back(tr);
        }
        return;
    } else {
        if (x >= noeud->gauche->carte.xmin && x <= noeud->gauche->carte.xmax &&
            y >= noeud->gauche->carte.ymin && y <= noeud->gauche->carte.ymax) {
            ajouterTriangle(noeud->gauche, profondeurActuelle + 1, tr, x, y);
        } else {
            ajouterTriangle(noeud->droit, profondeurActuelle + 1, tr, x, y);
        }
    }
}

std::vector<Triangle*> Arbre::trouverzone(double x, double y){
    return trouverzone(racine, 0, x, y) ;
}

void Arbre::ajouterTriangle(Triangle* tr){
    ajouterTriangle(racine, 0, tr, tr->x1, tr->y1) ;
    ajouterTriangle(racine, 0, tr, tr->x2, tr->y2) ;
    ajouterTriangle(racine, 0, tr, tr->x3, tr->y3) ;
}

void Arbre::parcourirArbre(){
    parcourirArbre(racine, 0) ;
}

void Arbre::diviserArbre() {
    diviserArbre(racine, 0);
}

void Arbre::detruireArbre(Noeud *noeud) {
    if (noeud != nullptr) {
        detruireArbre(noeud->gauche);
        detruireArbre(noeud->droit);
        delete noeud;
    }
}