#ifndef __POINT_H__
#define __POINT_H__

#include <vector>

using namespace std ;

class Point
{

public:
    double x, y, z ;
    Point(double x, double y) ; // Sans l'altitude
    Point(double x, double y, double z); // Avec l'altitude
    ~Point();
    bool pointDansTriangle(const Point& P1, const Point& P2, const Point& P3) ;
    vector<double> vecteur(const Point& P1, const Point& P2) ;
    vector<double> produitvectoriel(const Point& P1, const Point& P2, const Point& P3) ;
    vector<double> normaliser(vector<double>& v) ;
};

#endif