#include <iostream>
#include <iomanip>
#include <fstream>
#include <proj.h>
#include <fstream>
#include <vector>
#include <list>
#include <map>
#include <limits>
#include <chrono>
// Classes rajoutées /!\ aux boucles d'inclusions
#include "Arbre.h"
#include "Point.h"
#include "delaunay.hpp"
#include <math.h>
#include <thread>

using namespace std;

// Création du colormap Haxby
void haxby(int &r, int &g, int &b, double z, double zmin, double zmax)
{
  vector<int> cr = {37, 40, 50, 106, 138, 205, 240, 255, 255, 255, 255};
  vector<int> cg = {57, 127, 190, 235, 236, 255, 236, 189, 161, 186, 255};
  vector<int> cb = {175, 251, 255, 255, 174, 162, 121, 87, 68, 133, 255};

  int nbr = cr.size();

  vector<double> points(nbr);
  for (int i = 0; i < nbr; i++)
  {
      points[i] = zmin + ((zmax - zmin) / (nbr - 1)) * i;
  }

  for (int i = 0; i < nbr - 1; i++)
  {
    if (points[i] <= z && z <= points[i + 1])
    {
      r = (cr[i + 1] - cr[i]) / (points[i + 1] - points[i]) * (z - points[i]) + cr[i]; 
      g = (cg[i + 1] - cg[i]) / (points[i + 1] - points[i]) * (z - points[i]) + cg[i]; 
      b = (cb[i + 1] - cb[i]) / (points[i + 1] - points[i]) * (z - points[i]) + cb[i]; 
   }
  }
}

// Création d'un vecteur à partir de deux points
vector<double> vecteur(const Point& P1, const Point& P2) {
    vector<double> resultat(3) ;
    resultat[0] = P2.x - P1.x ;
    resultat[1] = P2.y - P1.y ;
    resultat[2] = P2.z - P1.z ;
    return resultat ;
}

// Produit vectoriel de deux vecteurs définis chacun par deux points
vector<double> produitvectoriel(const Point& P1, const Point& P2, const Point& P3) {
    vector<double> resultat(3) ;
    vector<double> v1 = vecteur(P1, P2) ;
    vector<double> v2 = vecteur(P1, P3) ;

    resultat[0] = v1[1]*v2[2] - v1[2]*v2[1] ;
    resultat[1] = v1[2]*v2[0] - v1[0]*v2[2] ;
    resultat[2] = v1[0]*v2[1] - v1[1]*v2[0] ;

    return resultat ;
}

// Normalisation d'un vecteur
vector<double> normaliser(vector<double>& v) {
    double length = std::sqrt(v[0] * v[0] + v[1] * v[1] + v[2] * v[2]);
    vector<double> result(3);
    result[0] = v[0] / length;
    result[1] = v[1] / length;
    result[2] = v[2] / length;
    return result;
}

// Programme principal
int main(int argc, char *argv[])
{

  // Gestion des paramètres d'entrée du programme
  if (argc != 3) {
    cerr << "Usage: " << argv[0] << " <input_file.txt> <image_width>" << endl;
    return EXIT_FAILURE;
  }

  // Récupération des données 
  const string file = argv[1];
  const int image_width = stoi(argv[2]);

  // Initialisation des référentiels de coordonnées :
  PJ* P = proj_create_crs_to_crs(
    PJ_DEFAULT_CTX,
    "+proj=longlat +datum=WGS84",
    "+proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs",
    NULL);

  // Ouverture du fichier
  const string& file_name = "../src/" + file ;
  ifstream f(file_name) ;

  if(!f.is_open())
    cout << "Impossible d'ouvrir le fichier en lecture" << endl;

  else{
    cout << "Fichier bien ouvert" << endl; 

    vector<double> coords_xy ; // Pour le triangulation
    map<pair<double, double>,double> alt ; // Pour la gestion des altitudes

    // Calcul des variables extrêmes du fichier
    float inf = numeric_limits<float>::infinity();
    double xmin, xmax, ymin, ymax, zmin, zmax;
    xmin = inf;
    ymin = inf;
    xmax = -inf;
    ymax = -inf;
    zmax = -inf;
    zmin = inf;

    string line ;
    while(getline(f, line)){

      // Deux coordonnées à exprimer dans des référentiels différents
      PJ_COORD geo_coord, cartesian_coord;

      // Lecture des coordonnées
      float lon, lat, z ;
      f >> lat >> lon >> z ;
      if (z < 0){
        z = -z ;
      }
      // Position géographique en latitude/longitude
      geo_coord.lpzt.lam = lon; // longitude
      geo_coord.lpzt.phi = lat; // latitude
      geo_coord.lpzt.z = 0.; // le z dans le référentiel ellipsoidale n'est pas nécessaire pour la projection

      // Projection géographique 
      cartesian_coord = proj_trans(P, PJ_FWD, geo_coord); // Projection en UTM zone 32 avec proj_trans : https://proj.org/en/9.3/development/quickstart.html

      // Sauvegarde des données dans un vector pour l'utilisation de delauntor.cpp
      coords_xy.push_back(cartesian_coord.xy.x) ;
      coords_xy.push_back(cartesian_coord.xy.y) ;

      // Sauvegarde des données dans alt
      alt[{cartesian_coord.xy.x, cartesian_coord.xy.y}] = z ;

      // Gestion des dimensions de la feuille
      if (xmin > cartesian_coord.xy.x) {
        xmin = cartesian_coord.xy.x ;
      }
      if (ymin > cartesian_coord.xy.y) {
        ymin = cartesian_coord.xy.y ;
      }
      if (xmax < cartesian_coord.xy.x) {
        xmax = cartesian_coord.xy.x ;
      }
      if (ymax < cartesian_coord.xy.y) {
        ymax = cartesian_coord.xy.y ;
      }
      if (zmax < z) {
          zmax = z;
      }
      if (z < zmin) {
          zmin = z;
      }
    }

  cout << "Ensemble des points lus et archivés" << endl ;

  // Calcul des dimensions de la carte
  double width = xmax - xmin ;
  double height = ymax - ymin ;
  cout << "Dimension de la carte : " << width << " m par " << height << " m" << endl ; 
  
  // Utilisation de delaunator.hpp pour la triangularisation
  auto start1 = chrono::high_resolution_clock::now() ;
  delaunator::Delaunator d(coords_xy) ;
  auto end1 = chrono::high_resolution_clock::now() ;
  auto duration1 = chrono::duration_cast<chrono::milliseconds>(end1 - start1).count();
  cout << "Triangularisation de Delaunay terminée en " << duration1 << " millisecondes" << endl ;

  // Création de l'arbre binaire pour stocker les données
  auto start2 = chrono::high_resolution_clock::now() ;
  Carte carteRacine(xmin, xmax, ymin, ymax);
  Arbre arbre(carteRacine, 18); // Profondeur maximale de l'arbre, 18 pour le lac de guerlédan, 10 pour la rade de Brest 
  arbre.diviserArbre();
  auto end2 = chrono::high_resolution_clock::now() ;
  auto duration2 = chrono::duration_cast<chrono::milliseconds>(end2 - start2).count();
  cout << "Découpage de l'arbre réussi en " << duration2 << " millisecondes" << endl ;

  // Stockage des données dans l'arbre binaire
  auto start3 = chrono::high_resolution_clock::now() ;
  std::map<Triangle*, double> m ;
  for(std::size_t i = 0; i < d.triangles.size(); i+=3) {
      double tx0 = d.coords[2 * d.triangles[i]] ;
      double ty0 = d.coords[2 * d.triangles[i] + 1] ;
      double tx1 = d.coords[2 * d.triangles[i + 1]] ;
      double ty1 = d.coords[2 * d.triangles[i + 1] + 1] ;
      double tx2 = d.coords[2 * d.triangles[i + 2]] ;
      double ty2 = d.coords[2 * d.triangles[i + 2] + 1] ;
      Triangle* tr = new Triangle(tx0, ty0, tx1, ty1, tx2, ty2);
      arbre.ajouterTriangle(tr); // Ajout d'un triangle
      int pourcentage = (i * 100) / d.triangles.size();

      // Affichage du pourcentage avec une barre de progression
      std::cout << "Insertion des triangles réalisée à " << pourcentage << "% [";
      int bar_size = pourcentage / 2; // Taille de la barre en fonction du pourcentage
      for (int j = 0; j < bar_size; ++j) {
          cout << "=";
      }
      for (int j = bar_size; j < 50; ++j) {
          cout << ".";
      }
      cout << "]" << flush;
      cout << '\r'; // Retour à la ligne (remplace la ligne précédente)
  }
  auto end3 = chrono::high_resolution_clock::now() ;
  auto duration3 = chrono::duration_cast<chrono::milliseconds>(end3 - start3).count();
  cout << "Insertion des triangles réussie en " << duration3 << " millisecondes" << endl ;

  // --- Création de l'image --- //

  // Dimensions
  auto start4 = chrono::high_resolution_clock::now() ;

  int largeur = image_width ;
  double resolution = largeur/width ;
  int hauteur = height * resolution ;
  
  // Format pgm
  string titre = "MNT_" + file.substr(0, file.length() - 4) ;
  titre = titre + ".pgm" ;
  
  std::ofstream fichierPGM(titre, std::ios::out | std::ios::binary);
  fichierPGM << "P6\n"
                << largeur << " " << hauteur << "\n255\n";

  // Azimuth and Altitude of the sun (adjust as needed)
  const double sunAzimuth = 0 ;
  const double sunAltitude = M_PI/4 ;

  // Vector representing the direction of the sun
  double sunDirectionX = cos(sunAzimuth) ;
  double sunDirectionY = sin(sunAzimuth) ;
  double sunDirectionZ = sin(sunAltitude) ;

  double num_pixel = 0;
  double nb_pixels = largeur * hauteur ;


  for (int y = hauteur; y >0; --y) {
    for (int x = 0; x < largeur; ++x) {
        num_pixel += 1;

        int pourcentage = (num_pixel * 100) / nb_pixels;

        // Affichage du pourcentage avec une barre de progression
        std::cout << "Image réalisée à " << pourcentage << "% [";
        int bar_size = pourcentage / 2; // Taille de la barre en fonction du pourcentage
        for (int j = 0; j < bar_size; ++j) {
            cout << "=";
        }
        for (int j = bar_size; j < 50; ++j) {
            cout << ".";
        }
        cout << "]" << flush;
        cout << '\r'; // Retour à la ligne (remplace la ligne précédente)

      // Calcul de la position du point sur la carte
      double carteX = xmin + (x * (xmax - xmin)) / largeur;
      double carteY = ymin + (y * (ymax - ymin)) / hauteur;
      Point p(carteX, carteY);

      // Recherche de la zone dans laquelle se trouve le point
      std::vector<Triangle*> list = arbre.trouverzone(carteX, carteY);
      bool pointInAnyTriangle = false;
      double altitudeMoyenne = 0.0; int nbtri = 0 ; double hillShadeFactor = 0 ;

      for (auto tri : list) {
        Point p1(tri->x1, tri->y1);
        Point p2(tri->x2, tri->y2);
        Point p3(tri->x3, tri->y3);

        if (p.pointDansTriangle(p1, p2, p3)) {
          // Utiliser l'altitude moyenne du triangle pour la couleur du pixel
          altitudeMoyenne += arbre.calculerAltitudeMoyenne(tri, alt) ; 
          nbtri += 1 ;
          pointInAnyTriangle = true;

          
          Point p1(tri->x1, tri->y1, alt[{tri->x1, tri->y1}]);
          Point p2(tri->x2, tri->y2, alt[{tri->x2, tri->y2}]);
          Point p3(tri->x3, tri->y3, alt[{tri->x3, tri->y3}]);

          vector<double> normale = produitvectoriel(p1, p2, p3) ;
          normale = normaliser(normale) ;

          double nx, ny, nz ;
          nx = normale[0] ;
          ny = normale[1] ;
          nz = normale[2] ;

          double illuminationFactor = nx * sunDirectionX + ny * sunDirectionY + nz * sunDirectionZ ;

          if (illuminationFactor > 0.0) {
            illuminationFactor = 0.0 ;
          }

          hillShadeFactor = illuminationFactor;

        }
      }

      if (!pointInAnyTriangle) {
        // Si le point n'est pas dans un triangle, colorer le pixel en blanc
        fichierPGM.put(static_cast<char>(255)); // Rouge 
        fichierPGM.put(static_cast<char>(255)); // Vert
        fichierPGM.put(static_cast<char>(255)); // Bleu
      }
      else {
        double z = altitudeMoyenne/nbtri ;
        int r, g, b;
        haxby(r, g, b, z, zmin, zmax);
        fichierPGM.put(static_cast<char>(r * (1 + 0.5 * hillShadeFactor))); // Rouge 
        fichierPGM.put(static_cast<char>(g * (1 + 0.5 * hillShadeFactor))); // Vert
        fichierPGM.put(static_cast<char>(b * (1 + 0.5 * hillShadeFactor))); // Bleu
      }
    }
  }

  fichierPGM.close();

  auto end4 = chrono::high_resolution_clock::now() ;
  auto duration4 = chrono::duration_cast<chrono::milliseconds>(end4 - start4).count();
  cout << endl << "Imagee réalisée en " << duration4 << " millisecondes" << endl ;
  }
  return EXIT_SUCCESS;
}