#ifndef __CARTE_H__
#define __CARTE_H__

/**
 * \class Carte
 * \brief Chaque noeud de l'arbre est représenté par une carte/sous-carte, ie. avec ses propres dimensions
 * \author Nicolas Damageux
 * \date 2023
*/

class Carte
{

    /**
     * \brief Crée une carte, à partir de ses 4 coordonnées
     * \param xmin Abscisse minimale des points de la carte 
     * \param xmax Abscisse maximale des points de la carte
     * \param ymin Ordonnée minimale des points de la carte 
     * \param ymax Ordonnée maximale des points de la carte
    */

public:
    double xmin, xmax, ymin, ymax ;

    Carte(double x1, double x2, double y1, double y2);
    ~Carte();
};

#endif